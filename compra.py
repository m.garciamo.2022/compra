#!/usr/bin/env python3

'''
Programa para mostrar la lista de la compra
'''

habitual = ("patatas", "leche", "pan")


def main():
    especifica = []  # Inicializamos la lista de compra específica vacía
    elemento = input("Elemento a comprar: ")

    while elemento != "":
        especifica.append(elemento)  # Agregamos el elemento a la lista de compra específica
        elemento = input("Elemento a comprar: ")

    # Unimos la compra habitual y específica y eliminamos duplicados
    lista_compra = list(set(habitual + tuple(especifica)))

    # Imprimimos la lista de la compra
    print("Lista de la compra:")
    for elemento in lista_compra:
        print(elemento)

    # Calculamos y mostramos el número de elementos
    elementos_habituales = len(habitual)
    elementos_especificos = len(especifica)
    elementos_totales = len(lista_compra)

    print(f"Elementos habituales: {elementos_habituales}")
    print(f"Elementos específicos: {elementos_especificos}")
    print(f"Elementos en lista: {elementos_totales}")


if __name__ == '__main__':
    main()